"""This package provides utilities to visualize the probability law of the number of desynchornization."""
from .cdf import cdf
